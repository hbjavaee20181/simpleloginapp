<%--
  Created by IntelliJ IDEA.
  User: Ben
  Date: 11/06/2018
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Dashboard</title>
</head>
<body>
<h1>Users List</h1>
<c:forEach items="${users}" var="u">
    <p>${u.firstName} - ${u.lastName} - ${u.email}</p>
</c:forEach>
</body>
</html>
