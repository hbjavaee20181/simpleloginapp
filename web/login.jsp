<%--
  Created by IntelliJ IDEA.
  User: Ben
  Date: 11/06/2018
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://getbootstrap.com/docs/4.1/examples/sign-in/signin.css">
</head>
<body class="text-center">

<form class="form-signin" method="POST">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <c:if test="${!empty error}">
        <p style="color: red">Login incorrect ! :(</p>
    </c:if>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input name="user_email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus=""
           type="email">
    <label for="inputPassword" class="sr-only">Password</label>
    <input name="user_password" id="inputPassword" class="form-control" placeholder="Password" required=""
           type="password">
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <a href="register">Pas inscrit? Faites le maintenant !</a>
</form>
</body>
</html>
