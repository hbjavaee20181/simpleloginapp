<%--
  Created by IntelliJ IDEA.
  User: Ben
  Date: 11/06/2018
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${sessionScope.isAuthenticated}">
    <c:redirect url="/app/dashboard"/>
</c:if>
<c:if test="${!sessionScope.isAuthenticated}">
    <c:redirect url="/login"/>
</c:if>