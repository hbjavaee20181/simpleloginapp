<%--
  Created by IntelliJ IDEA.
  User: Ben
  Date: 11/06/2018
  Time: 14:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://getbootstrap.com/docs/4.1/examples/sign-in/signin.css">
</head>
<body class="text-center">

<form class="form-signin" method="POST">
    <h1 class="h3 mb-3 font-weight-normal">Veuilllez remplir les champs</h1>
    <label for="lastname" class="sr-only">Nom</label>
    <input name="user_lastname" id="lastname" class="form-control" placeholder="Nom" required="" autofocus=""
           type="text">
    <label for="firstname" class="sr-only">Prénom</label>
    <input name="user_firstname" id="firstname" class="form-control" placeholder="Prénom" required="" autofocus=""
           type="text">
    <label for="email" class="sr-only">Adresse email</label>
    <input name="user_email" id="email" class="form-control" placeholder="Email address" required="" autofocus=""
           type="email">
    <label for="password" class="sr-only">Mot de passe</label>
    <input name="user_password" id="password" class="form-control" placeholder="Password" required="" type="password">
    <button class="btn btn-lg btn-primary btn-block" type="submit">Créer mon compte</button>
</form>
</body>
</html>
