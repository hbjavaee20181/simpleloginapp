package com.humanbooster.authapp.dao.impl;

import com.humanbooster.authapp.constant.SQLConstants;
import com.humanbooster.authapp.dao.UserDao;
import com.humanbooster.authapp.exception.AuthApplicationException;
import com.humanbooster.authapp.model.User;
import org.jooq.DSLContext;
import org.jooq.Record5;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class JooqUserDao implements UserDao {

    private DSLContext ctx;

    public JooqUserDao(Connection connection) {
        ctx = DSL.using(connection, SQLDialect.MARIADB);
    }

    @Override
    public User findUserByEmail(String email) {
        Record5<Long, String, String, String, String> result = ctx
                .select(
                        DSL.field(SQLConstants.ID_COLUMN).cast(Long.class),
                        DSL.field(SQLConstants.FIRSTNAME_COLUMN).cast(String.class),
                        DSL.field(SQLConstants.LASTNAME_COLUMN).cast(String.class),
                        DSL.field(SQLConstants.EMAIL_COLUMN).cast(String.class),
                        DSL.field(SQLConstants.PASSWORD_COLUMN).cast(String.class)
                )
                .from(SQLConstants.TABLE_USERS)
                .where(DSL.field(SQLConstants.EMAIL_COLUMN).eq(email))
                .fetchOne();

        if (Objects.isNull(result)) {
            // No user with this email, we return null
            return null;
        }
        ;

        return new User(
                result.component1(),
                result.component2(),
                result.component3(),
                result.component4(),
                result.component5()
        );
    }

    @Override
    public User findUserById(Long id) {
        Record5<Long, String, String, String, String> result = ctx
                .select(
                        DSL.field(SQLConstants.ID_COLUMN).cast(Long.class),
                        DSL.field(SQLConstants.FIRSTNAME_COLUMN).cast(String.class),
                        DSL.field(SQLConstants.LASTNAME_COLUMN).cast(String.class),
                        DSL.field(SQLConstants.EMAIL_COLUMN).cast(String.class),
                        DSL.field(SQLConstants.PASSWORD_COLUMN).cast(String.class)
                )
                .from(SQLConstants.TABLE_USERS)
                .where(DSL.field(SQLConstants.ID_COLUMN).eq(id))
                .fetchOne();

        if (Objects.isNull(result)) {
            // No user with this email, we return null
            return null;
        }
        ;

        return new User(
                result.component1(),
                result.component2(),
                result.component3(),
                result.component4(),
                result.component5()
        );
    }

    @Override
    public void addUser(User user) {
        // @COPYRIGHT STEPHANE
        //  Define table & columns
        ctx.insertInto(
                DSL.table(SQLConstants.TABLE_USERS),
                DSL.field(SQLConstants.EMAIL_COLUMN),
                DSL.field(SQLConstants.PASSWORD_COLUMN),
                DSL.field(SQLConstants.FIRSTNAME_COLUMN),
                DSL.field(SQLConstants.LASTNAME_COLUMN)
        )
                //  Set values from user
                .values(
                        user.getEmail(),
                        user.getPassword(),
                        user.getFirstName(),
                        user.getLastName()
                )
                .execute();
    }

    @Override
    public List<User> getAllUsers() {
        ResultSet resultSet = ctx
                .select(
                        DSL.field(SQLConstants.ID_COLUMN),
                        DSL.field(SQLConstants.FIRSTNAME_COLUMN),
                        DSL.field(SQLConstants.LASTNAME_COLUMN),
                        DSL.field(SQLConstants.EMAIL_COLUMN),
                        DSL.field(SQLConstants.PASSWORD_COLUMN))
                .from(DSL.table(SQLConstants.TABLE_USERS))
                .fetchResultSet();

        List<User> users = new ArrayList<>();

        try {
            while (resultSet.next()) {
                Long id = resultSet.getLong(SQLConstants.ID_COLUMN);
                String firstName = resultSet.getString(SQLConstants.FIRSTNAME_COLUMN);
                String lastName = resultSet.getString(SQLConstants.LASTNAME_COLUMN);
                String email = resultSet.getString(SQLConstants.EMAIL_COLUMN);
                String password = resultSet.getString(SQLConstants.PASSWORD_COLUMN);

                User u = new User(id, firstName, lastName, email, password);

                users.add(u);
            }
        } catch (SQLException e) {
            throw new AuthApplicationException(e);
        }

        return users;
    }
}
