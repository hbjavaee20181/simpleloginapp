package com.humanbooster.authapp.dao;

import com.humanbooster.authapp.dao.impl.JooqUserDao;
import com.humanbooster.authapp.sql.ConnectionManager;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class DaoFactory {

    private DaoFactory() {

    }

    public static UserDao getUserDao() {
        return new JooqUserDao(ConnectionManager.getConnection());
    }
}
