package com.humanbooster.authapp.dao;

import com.humanbooster.authapp.model.User;

import java.util.List;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface UserDao {

    User findUserByEmail(String email);

    User findUserById(Long id);

    void addUser(User user);

    List<User> getAllUsers();
}
