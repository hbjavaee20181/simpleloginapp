package com.humanbooster.authapp.sql;


import com.humanbooster.authapp.constant.SQLConstants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class ConnectionManager {

    private ConnectionManager() {

    }

    private static Connection connection;

    public static boolean initConnection() {
        return getConnection() != null;
    }

    // Warning, not totally thread safe
    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(
                        SQLConstants.DB_URL,
                        SQLConstants.DB_USER,
                        SQLConstants.DB_PASSWORD
                );
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    public static void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
