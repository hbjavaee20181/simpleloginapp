package com.humanbooster.authapp.constant;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class Constants {

    public static final String IS_AUTH = "isAuthenticated";
}
