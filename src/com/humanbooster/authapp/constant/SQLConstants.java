package com.humanbooster.authapp.constant;

/**
 * Created by Ben on 06/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class SQLConstants {

    private SQLConstants() {

    }

    public static final String DB_URL = "jdbc:mariadb://localhost:32768/auth_app";

    public static final String DB_USER = "root";

    public static final String DB_PASSWORD = "root";

    public static final String TABLE_USERS = "users";

    public static final String ID_COLUMN = "id";
    public static final String FIRSTNAME_COLUMN = "first_name";
    public static final String LASTNAME_COLUMN = "last_name";
    public static final String EMAIL_COLUMN = "email";
    public static final String PASSWORD_COLUMN = "password";
}
