package com.humanbooster.authapp.exception;

/**
 * Created by Ben on 12/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class ApplicationCriticalException extends RuntimeException {

    public ApplicationCriticalException(Exception e) {
        super(e);
    }
}
