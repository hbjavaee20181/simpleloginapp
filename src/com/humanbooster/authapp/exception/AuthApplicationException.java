package com.humanbooster.authapp.exception;

/**
 * Created by Ben on 12/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public class AuthApplicationException extends RuntimeException {

    public AuthApplicationException(Exception e) {
        super(e);
    }
}
