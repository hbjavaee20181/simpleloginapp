package com.humanbooster.authapp.servlet;

import com.humanbooster.authapp.constant.Constants;
import com.humanbooster.authapp.dao.DaoFactory;
import com.humanbooster.authapp.model.User;
import com.humanbooster.authapp.utils.HashUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //TODO Redirect the user if authenticated
        req.getRequestDispatcher("/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("user_email");
        String password = req.getParameter("user_password");

        User user = DaoFactory.getUserDao().findUserByEmail(email);

        if (user == null) {
            req.setAttribute("error", true);
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
            return;
        }

        String hashedPassword = HashUtils.hashPassword(password);

        if (!user.getPassword().equals(hashedPassword)) {
            req.setAttribute("error", true);
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }

        HttpSession session = req.getSession(true);
        session.setAttribute(Constants.IS_AUTH, true);

        resp.sendRedirect(getServletContext().getContextPath() + "/app/dashboard");
    }
}
