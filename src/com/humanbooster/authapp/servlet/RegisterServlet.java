package com.humanbooster.authapp.servlet;

import com.humanbooster.authapp.dao.DaoFactory;
import com.humanbooster.authapp.model.User;
import com.humanbooster.authapp.utils.HashUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@WebServlet(urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("user_firstname");
        String lastName = req.getParameter("user_lastname");
        String email = req.getParameter("user_email");
        String hashedPassword = HashUtils.hashPassword(req.getParameter("user_password"));

        User newUser = new User(firstName, lastName, email, hashedPassword);

        DaoFactory.getUserDao().addUser(newUser);

        resp.sendRedirect(getServletContext().getContextPath() + "/login");
    }
}
