package com.humanbooster.authapp.servlet;

import com.humanbooster.authapp.dao.DaoFactory;
import com.humanbooster.authapp.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@WebServlet(urlPatterns = "/app/dashboard")
public class DashboardServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> allUsers = DaoFactory.getUserDao().getAllUsers();
        req.setAttribute("users", allUsers);
        req.getRequestDispatcher("/app/dashboard.jsp").forward(req, resp);
    }
}
