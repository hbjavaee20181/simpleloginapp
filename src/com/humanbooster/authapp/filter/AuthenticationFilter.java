package com.humanbooster.authapp.filter;

import com.humanbooster.authapp.constant.Constants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Ben on 11/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@WebFilter(urlPatterns = "/app/*")
public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to do
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(true);

        Boolean isAuth = (Boolean) session.getAttribute(Constants.IS_AUTH);

        if (isAuth != null && isAuth) {
            chain.doFilter(request, response);
        } else {
            resp.sendRedirect(req.getServletContext().getContextPath() + "/login");
        }
    }

    @Override
    public void destroy() {
        // Nothing to do
    }
}
