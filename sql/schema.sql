-- Create the database
CREATE database auth_app;

-- Switch to the database
USE auth_app;

-- Create the table
CREATE TABLE users (
     id INT NOT NULL AUTO_INCREMENT,
     first_name CHAR(50) NOT NULL,
     last_name CHAR(50) NOT NULL,
     email CHAR(50) NOT NULL,
     password VARCHAR(255) NOT NULL,
     PRIMARY KEY (id)
 );